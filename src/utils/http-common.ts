import axios from "axios";

export default axios.create({
    baseURL : "http://localhost:8080/api/v1",
    headers : {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json"
    }
})