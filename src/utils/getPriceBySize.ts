import { pizzaType } from "../types/pizzaType";

export const getPriceBySize = (size: string, pizzaData: pizzaType): number => {
    switch (size) {
        case 'Small':
            return pizzaData.priceRegular;
        case 'Medium':
            return pizzaData.priceMedium;
        case 'Large':
            return pizzaData.priceLarge;
        default:
            return 0;
    }
};