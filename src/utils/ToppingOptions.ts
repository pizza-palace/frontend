export const VegTopings = [
    "Black Olive",
    "Capsicum",
    "Paneer" ,
    "Mushroom" ,
    "Fresh Tomato",
    "Extra Cheese"
]


export const NonVegToppings = [
    "Chicken Tikka" ,
    "Barbeque Chicken" ,
    "Grilled Chicken" ,
    "Mushroom" ,
    "Fresh Tomato",
    "Black Olive" ,
    "Capsicum" ,
    "Extra Cheese" 
]

export const AllToppings : { [key: string]: number } = {
    "Black Olive" : 20,
    "Capsicum" : 25,
    "Paneer" : 35,
    "Mushroom" : 30,
    "Fresh Tomato" : 10,
    "Extra Cheese" : 35,
    "Chicken Tikka" : 35,
    "Barbeque Chicken" : 45,
    "Grilled Chicken" : 40,
}
