const usedIds = new Set<number>();

export const generateNewSubId = () => {
    let newId;
    do {
        newId = Math.floor(Math.random() * 1000000); // Increase range to 0 to 999,999
    } while (usedIds.has(newId));
    usedIds.add(newId);
    return newId;
}
