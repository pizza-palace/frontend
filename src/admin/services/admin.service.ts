import { Pizza } from "../../types/PizzaForm";
import http from "../../utils/http-common"
class AdminService{

    getAllPizza(){
        return http.get("/pizza");
    }
    deletePizza(id : number){
        return http.delete("/pizza/"+id);
    }
    createPizza(data:Pizza){
        return http.post("/pizza", data);
    }
    updatePizza(data:Pizza, id:number){
        return http.put("/pizza/"+id, data);
    }
    getOrders(){
        return http.get("/order");
    }
    updateStatus(data:any, id:number | undefined){
        return http.put("/order/"+id, data);
    }
}

export default new AdminService();