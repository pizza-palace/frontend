import { useEffect, useState } from 'react'
import adminService from '../services/admin.service';
import { OrderType } from '../../types/OrderType';

export const Orders = () => {
  const [orders, setOrders] = useState<OrderType[]>([]);
  const [status, setStatus] = useState<string>();

  const handleStatusChange = (value: string, id: number | undefined) => {
    adminService.updateStatus({ status: value }, id)
      .then((res) => {
        console.log(res);
        setStatus(value);
      })
      .catch((err) => {
        console.log(err);

      })
  }

  useEffect(() => {
    adminService.getOrders()
      .then((res: any) => {
        setOrders(res.data.data);
      })
      .catch((err) => {
        console.log(err);

      })
  }, [status])

  return (
    <div style={{ height: "100vh", paddingTop: "6rem" }} className='d-flex flex-column align-items-center'>
      <p className='fs-3 fw-bold'>All Orders</p>
      <table className="table table-borderless w-75">
        <thead>
          <tr>
            <th scope="col">Order Id</th>
            <th scope="col">Customer Id</th>
            <th scope="col">Total Amount</th>
            <th scope="col">Date Time</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {
            orders.map((item, index) => (
              <tr key={index}>
                <td>{item.orderId}</td>
                <td>{item.customerId}</td>
                <td>&#8377;{item.totalAmount}</td>
                <td>{item.orderDateTime}</td>
                <td>
                  <select className="border-dark rounded-1 p-1" value={item.status} onChange={(event: React.ChangeEvent<HTMLSelectElement>) => { handleStatusChange(event.target.value, item.orderId) }}>
                    <option value="CREATED">CREATED</option>
                    <option value="PREPARING">PREPARING</option>
                    <option value="PREPARED">PREPARED</option>
                    <option value="OUT FOR DELIVERY">OUT FOR DELIVERY</option>
                    <option value="DELIVERED">DELIVERED</option>
                  </select>

                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  )
}
