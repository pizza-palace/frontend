import { ChangeEvent, useEffect, useState } from "react";
import { pizzaType } from "../../types/pizzaType";
import adminService from "../services/admin.service";
import { Pizza } from "../../types/PizzaForm";
import { AxiosError } from "axios";
import { ToastContainer, toast } from "react-toastify";

export const Home = () => {
  const [exist, setExist] = useState<boolean>(false);
  const [id, setId] = useState<number>(0);
  const [pizzaList, setPizzaList] = useState<pizzaType[]>([]);
  const [pizzaListChanged, setPizzaListChanged] = useState<boolean>(false);

  const [pizza, setPizza] = useState<Pizza>({
    name: '',
    pizzaType: 'Veg',
    description: '',
    imageURL: '',
    priceRegular: '',
    priceMedium: '',
    priceLarge: '',
  });
  
  useEffect(() => {
    adminService.getAllPizza().then((res : any) => {
      setPizzaList(res.data.data);
    })

  }, [pizzaListChanged])

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
    const { name, value } = e.target;
    setPizza({ ...pizza, [name]: name.includes('price') ? Number(value) : value });
  };

  const handleSubmit = async () => {
    console.log('Pizza data:', pizza);
    await adminService.createPizza(pizza)
      .then((res) => {
        setPizzaListChanged(!pizzaListChanged);
        setPizzaToDefault();
      })
      .catch((err) => {
        if( err instanceof AxiosError && err){
          err.response?.data.error.message.forEach((item:string)=>{
            toast.error(item);
          })
          
        }
      })
  };

  const handleClickOnEdit = (id: number) => {
    setExist(true);
    setId(id);
    pizzaList.map((item) => {
      if (item.pizzaId === id) {
        setPizza({
          name: item.name,
          pizzaType: item.pizzaType,
          description: item.description,
          imageURL: item.imageURL,
          priceRegular: item.priceRegular,
          priceMedium: item.priceMedium,
          priceLarge: item.priceLarge
        })
      }
    })
  }

  const handleUpdate = async () => {
    adminService.updatePizza(pizza, id)
      .then((res) => {
        setPizzaListChanged(!pizzaListChanged);
      })
      .catch((err) => {
        console.log(err);

      })
    setPizzaToDefault();
  }

  const handleDelete = async (id: number) => {
    adminService.deletePizza(id)
      .then((res) => {
        toast.success("pizza data deleted with the id : " + id)
        setPizzaListChanged(!pizzaListChanged);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  const setPizzaToDefault = () => {
    setPizza({
      name: '',
      pizzaType: '',
      description: '',
      imageURL: '',
      priceRegular: '',
      priceMedium: '',
      priceLarge: '',
    })
  }
  return (
    <div style={{ paddingTop: "6rem" }} className="d-flex justify-content-around">
      <ToastContainer/>
      <div className="w-50">
        <p className="text-center fs-2">All Pizza List</p>
        <table className="table table-bordered shadow">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Type</th>
              <th scope="col">Description</th>
              <th scope="col">Large</th>
              <th scope="col">Regular</th>
              <th scope="col">Medium</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>

            {
              pizzaList.map((item, index) => (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td>{item.pizzaType}</td>
                  <td>{item.description}</td>
                  <td>{item.priceLarge}</td>
                  <td>{item.priceRegular}</td>
                  <td>{item.priceMedium}</td>
                  <td>
                    <span onClick={() => { handleDelete(item.pizzaId) }} className="text-danger material-symbols-outlined pointer">
                      delete
                    </span>
                    <br />
                    <span onClick={() => { handleClickOnEdit(item.pizzaId) }} className="text-warning material-symbols-outlined pointer pt-4">
                      edit
                    </span>
                  </td>
                </tr>
              ))
            }

          </tbody>
        </table>
      </div>

      <div>
        <p className="fs-2 text-center">Add Pizza</p>
        <form className="d-flex flex-column gap-2">
          <input
            className="form-control"
            type="text"
            name="name"
            placeholder="Pizza Name"
            value={pizza.name}
            onChange={handleChange}
          />
          <input
            className="form-control"
            type="text"
            name="imageURL"
            placeholder="Image URL"
            value={pizza.imageURL}
            onChange={handleChange}
          />
          <select
            className="form-control"
            name="pizzaType"
            value={pizza.pizzaType}
            onChange={handleChange}
          >
            <option defaultValue={"Veg"} value="Veg">Veg</option>
            <option value="Non Veg">Non Veg</option>
          </select>
          <textarea
            className="form-control"
            name="description"
            placeholder="Enter pizza description"
            value={pizza.description}
            onChange={handleChange}
          />
          <input
            className="form-control"
            type="number"
            name="priceRegular"
            placeholder="Enter Regular pizza price"
            value={pizza.priceRegular}
            onChange={handleChange}
          />
          <input
            className="form-control"
            type="number"
            name="priceMedium"
            placeholder="Enter Medium pizza price"
            value={pizza.priceMedium}
            onChange={handleChange}
          />
          <input
            className="form-control"
            type="number"
            name="priceLarge"
            placeholder="Enter Large pizza price"
            value={pizza.priceLarge}
            onChange={handleChange}
          />
          <button type="button" className="btn btn-primary" onClick={exist ? handleUpdate : handleSubmit}>
            Add
          </button>
        </form>
      </div>
    </div>
  )
}
