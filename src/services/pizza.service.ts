import { OrderLineType } from "../types/OrderLineType";
import { OrderType } from "../types/OrderType";
import { pizzaType } from "../types/pizzaType"
import http from "../utils/http-common"

class PizzaDataService{
    getAllPizza(){
        return http.get<pizzaType>("/pizza");
    }

    createOrder(data:OrderType){
        return http.post("/order", data);
    }

    deleteOrder(id : number | undefined){
        return http.delete("/order/"+id);
    }

    deleteOrderLine(orderId : number | undefined){
        return http.delete("/order-line/"+orderId);
    }

    createOrderLine(data : OrderLineType){
        return http.post("/order-line", data);
    }

    getAllPizzaOrdersByCustomerId(customerId : number){
        return http.get<OrderType>("/order/orders-by-customer/"+customerId);
    }

    getOrder(orderId : number | undefined){
        return http.get<OrderType>("/order/"+orderId);
    }

    getAllOrderLineByOrderId(orderId : number | undefined){
        return http.get<OrderLineType>("/order-line/order-line-by-order/"+orderId);
    }

    updateOrder(orderId : number | undefined, order : OrderType){
        return http.put("/order/"+orderId, order);
    }

    updateOrderLine(orderLineId : number | undefined, orderLine : OrderLineType ){
        return http.put("/order-line/"+orderLineId, orderLine);
    }

    deleteOrderLineByOrderLineId(orderLineId : number | undefined){
        return http.delete("/order-line/by-order-line-id/"+orderLineId);
    }
}

export default new PizzaDataService();