import { SignupType } from "../types/SignupType";
import http from "../utils/http-common";

class UserService{
    createUser(data:SignupType){
        return http.post("/customer", data);
    }
    getUser(email:string){
        return http.get("/customer/email/"+email);
    }
    getAdmin(email : string){
        return http.get("/admin/"+email);
    }
}

export default new UserService();