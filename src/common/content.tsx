import { Routes, Route } from "react-router-dom";
import MainPage from "../pages/mainPage";
import MainCart from "../pages/mainCart";
import { MyOrders } from "../pages/MyOrders";
import { Home } from "../admin/pages/AdminHome";
import { Orders } from "../admin/pages/Orders";
import Login from "../pages/Login";
import Signup from "../pages/Signup";
import ProtectedRoute from "../components/ProtectedRoute";
import AdminRoute from "../components/AdminRoute";

const Content = () => {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="/login" element={<Login />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/cart" element={<MainCart />} />
      <Route
        path="/orders"
        element={
          <ProtectedRoute>
            <MyOrders />
          </ProtectedRoute>
        }
      />
      <Route path="/admin">
        <Route
          path="home"
          element={
            <AdminRoute>
              <Home />
            </AdminRoute>
          }
        />
        <Route
          path="orders"
          element={
            <AdminRoute>
              <Orders />
            </AdminRoute>
          }
        />
      </Route>
    </Routes>
  );
};

export default Content;








































// import {
//     Routes,
//     Route,
// } from "react-router-dom";
// import MainPage from "../pages/mainPage";
// import MainCart from "../pages/mainCart";
// import { MyOrders } from "../pages/MyOrders";
// import { Home } from "../admin/pages/AdminHome";
// import { Orders } from "../admin/pages/Orders";
// import  Login  from "../pages/Login";
// import Signup from "../pages/Signup";
// const Content = () => {
//     return (
//         <Routes>
//             <Route path="/" Component={MainPage}></Route>
//             <Route path="/login" Component={Login}></Route>
//             <Route path="/signup" Component={Signup}></Route>
//             <Route path="/cart" Component={MainCart}></Route>
//             <Route path="/orders" Component={MyOrders}></Route>
//             <Route path="/admin">
//                 <Route path="home" Component={Home}></Route>
//                 <Route path="orders" Component={Orders}></Route>
//             </Route>
//         </Routes>
//     )
// }

// export default Content;