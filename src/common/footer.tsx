
const Footer = () =>
 {
    return(
        <footer className="page-footer font-small blue mt-4" style={{ 
            backgroundColor: `#FFF`,paddingTop:"5px"
          }}>
            <div className="container-fluid text-center text-md-left mt-3">
                <div className=" d-flex justify-content-around" >
                    <div className="col-md-3 mb-md-0 mb-3">
                        <h5 className="text-uppercase">Contact us</h5>
                        <ul className="list-unstyled">
                            <li><a href="#!">PizzaHub@pizza.com</a></li>
                            <li><p>8080808080</p></li>
                          
                        </ul>
                    </div>
                    <div className="col-md-3 mb-md-0 mb-3">
                        <h5 className="text-uppercase">Address</h5>
                        <ul className="list-unstyled">
                            <li>180 Madam Cama Road, Opposite Air India Building, Nariman Point, Mumbai, Maharashtra 400020, India</li>                         
                        </ul>
                    </div>
                </div>
            </div>
        
            <div className="footer-copyright text-center py-3">© 2023 Copyright:
                <a href="www.google.com"> PizzaHub.com</a>
            </div>
        
        </footer>
    )
 }

export default Footer