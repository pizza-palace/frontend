
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import { useCartContext } from '../context/cart/UseCartContext';
import { useUserContext } from '../context/user/UseUserContext';
import { useAuth } from '../context/Auth/AuthContext';

const NavbarComponent = () => {
    const { cartData } = useCartContext();
    const { isAuthenticated, logout, isAdmin, removeAdmin } = useAuth();
    const { removeUser } = useUserContext();
    const navigate = useNavigate();

    const location = useLocation();

    return (
        <Navbar fixed="top" className="bg-body-tertiary p-2 shadow-sm">
            <Container>
                <Navbar.Brand style={{ cursor: "pointer" }} onClick={() => navigate("/")} className='text-primary fw-bolder fs-3'>PizzaHub</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    {
                        (isAdmin && /^\/admin\/(home|orders)$/.test(location.pathname))
                            ?
                            <div className='d-flex gap-3'>
                                <Navbar.Text>
                                    <NavLink className="text-dec-none" to={"/admin/home"}>Home</NavLink>
                                </Navbar.Text>
                                <Navbar.Text>
                                    <NavLink className="text-dec-none" to={"/"}>UserHome</NavLink>
                                </Navbar.Text>
                                <Navbar.Text>
                                    <NavLink className="text-dec-none" to={"/admin/orders"}>Orders</NavLink>
                                </Navbar.Text>
                                <Navbar.Text className='me-3'>
                                    <NavLink className="text-dec-none" onClick={() => { removeUser(); logout(); removeAdmin(); }} to={'/'}>Logout</NavLink>
                                </Navbar.Text>
                            </div>
                            :
                            <>
                                {
                                    isAuthenticated
                                        ?
                                        isAdmin
                                            ?
                                            <>
                                                <Navbar.Text className='me-3'>
                                                    <NavLink className="text-dec-none" to={'/orders'}>My Orders</NavLink>
                                                </Navbar.Text>
                                                <Navbar.Text className='me-3'>
                                                    <NavLink className="text-dec-none" to={'/admin/home'}>Dashboard</NavLink>
                                                </Navbar.Text>
                                                <Navbar.Text className='me-3'>
                                                    <NavLink className="text-dec-none" onClick={() => { removeUser(); logout(); removeAdmin(); }} to={'/'}>Logout</NavLink>
                                                </Navbar.Text>
                                            </>
                                            :
                                            <>
                                                <Navbar.Text className='me-3'>
                                                    <NavLink className="text-dec-none" to={'/orders'}>My Orders</NavLink>
                                                </Navbar.Text>
                                                <Navbar.Text className='me-3'>
                                                    <NavLink className="text-dec-none" onClick={() => { removeUser(); logout(); removeAdmin(); }} to={'/'}>Logout</NavLink>
                                                </Navbar.Text>
                                            </>
                                        :
                                        <>
                                            <Navbar.Text className='me-3'>
                                                <NavLink className="text-dec-none" to={'/login'}>Login</NavLink>
                                            </Navbar.Text>
                                            <Navbar.Text className='me-3'>
                                                <NavLink className="text-dec-none" to={'/signup'}>Signup</NavLink>
                                            </Navbar.Text>
                                        </>
                                }


                                <Navbar.Text>
                                    <div className='cart-icon p-2'>
                                        <span style={{ cursor: "pointer" }} onClick={() => navigate("/cart")} className="material-symbols-outlined">
                                            shopping_cart
                                        </span>
                                        {cartData.length == 0 ? <span></span> : <div style={{ fontSize: "0.8rem" }} className='dot bg-danger text-light fw-bold text-center'>{cartData.length}</div>}
                                    </div>
                                </Navbar.Text>
                            </>
                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavbarComponent;