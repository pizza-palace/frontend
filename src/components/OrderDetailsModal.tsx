import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { OrderLineType } from '../types/OrderLineType';
import pizzaService from '../services/pizza.service';
import { UsePizzaContext } from '../context/pizza/UsePizzaContext';

type PropType = {
  id: number | undefined
}

function OrderDetailsModal({ id }: PropType) {
  const { getPizza } = UsePizzaContext();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [orderLineList, setOrderLineList] = useState<OrderLineType[]>([]);

  useEffect(() => {
    pizzaService.getAllOrderLineByOrderId(id)
        .then((res: any) => {
          const fetchedOrderLines = res.data.data.map((line: OrderLineType) => ({
            ...line,
            toppings: line.toppings || "" 
          }));
          setOrderLineList(fetchedOrderLines);
        })
        .catch((err) => {
          console.log(err);
        });
  }, [orderLineList])


  return (
    <>
      <Button size='sm' variant="outline-primary" onClick={handleShow}>
        Details
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Body className='d-flex gap-2 flex-column'>
          <p className='fw-bold text-center' style={{fontSize:"0.9rem"}}>Pizza Name | Quantity | Total Pice | Size | crust | Type</p>
          {
            orderLineList.map((item, index) => {
              let pizza = getPizza(item.pizzaId);
              return (
                <div key={index} className='border border-1 p-1 rounded-2 d-flex flex-column align-items-center'>
                  <p style={{fontSize:"0.7rem"}}>{pizza?.name} | {item.quantity} | &#8377;{item.totalPrice} | {item.pizzaSize} | {item.crust} | {pizza?.pizzaType}</p>
                  <p style={{fontSize:"0.7rem"}}>Added toppings : {item.toppings.split("-").map((topping)=>(<>{topping}, </>))}</p>
                </div>
              )
            })
          }
          <div>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default OrderDetailsModal;