import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { OrderType } from '../types/OrderType';
import React, { useEffect, useState } from 'react';
import pizzaService from '../services/pizza.service';
import { OrderLineType } from '../types/OrderLineType';
import { UsePizzaContext } from '../context/pizza/UsePizzaContext';
import { getPriceBySize } from '../utils/getPriceBySize';
import { QuantityInput } from './QuantityInput';
import { AllToppings, NonVegToppings, VegTopings } from '../utils/ToppingOptions';

type PropType = {
  isEditable: (orderDateTime: string) => boolean
  id: number | undefined
  orderDateTime: string
}

const UpdateOrderModal = ({ isEditable, orderDateTime, id }: PropType) => {
  const { getPizza } = UsePizzaContext();
  const [show, setShow] = useState(false);
  const [order, setOrder] = useState<OrderType>({} as OrderType);
  const [address, setAddress] = useState<string>("");
  const [orderLine, setOrderLine] = useState<OrderLineType[]>([]);
  const [isChanged, setIsChanged] = useState<boolean>(false);

  useEffect(() => {
    if (id !== undefined) {
      pizzaService.getOrder(id)
        .then((res: any) => {
          setOrder(res.data.data);
          setAddress(res.data.data.deliveryAddress);
        })
        .catch((err) => {
          console.log(err);
        })

        pizzaService.getAllOrderLineByOrderId(id)
        .then((res: any) => {
          const fetchedOrderLines = res.data.data.map((line: OrderLineType) => ({
            ...line,
            toppings: line.toppings || "" 
          }));
          setOrderLine(fetchedOrderLines);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [id, isChanged]);



  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleAddressChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setAddress(event.target.value);
  }

  const handleOrderLineChange = (index: number, field: string, value: any) => {
    const updatedOrderLine = [...orderLine];
    updatedOrderLine[index] = { ...updatedOrderLine[index], [field]: value };
    setOrderLine(updatedOrderLine);
  }

  const handleDeleteOrderLine = (index: number | undefined) => {
    pizzaService.deleteOrderLineByOrderLineId(index)
      .then((res) => {
        console.log("deleted successfully");

        const updatedOrderLine = orderLine.filter((_, i) => i !== index);
        setOrderLine(updatedOrderLine);
        setIsChanged(true);
      })
      .catch((err) => {
        console.log(err);

      })
  };

  const handleToppingChange = (index: number, topping: string, isChecked: boolean) => {
    const updatedOrderLine = [...orderLine];
    if(updatedOrderLine[index].toppings !== null){
      let toppingsArray = updatedOrderLine[index].toppings ? updatedOrderLine[index].toppings.split('-') : [];

    if (isChecked) {
      toppingsArray.push(topping);
    } else {
      toppingsArray = toppingsArray.filter(t => t !== topping);
    }

    updatedOrderLine[index] = { ...updatedOrderLine[index], toppings: toppingsArray.join('-') };
    setOrderLine(updatedOrderLine);
    }
  }

  const handleSubmit = async () => {
    let updatedAmount = 0;
    orderLine.map((item) => {
      const pizza = getPizza(item.pizzaId);
      if (pizza) {
        let price = getPriceBySize(item.pizzaSize, pizza);
        item.totalPrice = price;
        updatedAmount += (price * item.quantity);
      }
      let toppingArray = item.toppings.split("-");
      for(let i=0; i<toppingArray.length; i++){
        if(toppingArray[i].length > 0) updatedAmount += AllToppings[toppingArray[i]];
      }
      
    })

    const updatedOrder = { ...order, deliveryAddress: address, totalAmount: updatedAmount };



    await pizzaService.updateOrder(order.orderId, updatedOrder)
      .then((res) => {

      })
      .catch((err) => {
        console.log(err);
      })

    orderLine.map(async (item) => {
      await pizzaService.updateOrderLine(item.orderLineId, item)
        .then((res) => {

        })
        .catch((err) => {
          console.log(err);
        })
    })

  }


  return (
    <>
      <span onClick={handleShow} style={isEditable(orderDateTime) ? {} : { display: "none" }} className="material-symbols-outlined text-warning pointer">
        edit
      </span>
      <Modal show={show} onHide={handleClose}>
        <Modal.Body className='d-flex flex-column'>
          <div className='d-flex flex-column gap-2'>
            <textarea className='form-control' value={address} onChange={handleAddressChange} placeholder="Delivery Address" />
            {
              orderLine.map((item, index) => {
                let pizza = getPizza(item.pizzaId);
                let toppingsArray = item.toppings ? item.toppings.split("-") : [];
                // console.log(typeof(item.toppings));

                return (
                  <div id='update-order-div' key={index} className='d-flex flex-column gap-1 align-items-center justify-content-between border border-black p-2 rounded-1'>
                    <p className='fw-bold'>{pizza?.name}</p>
                    <div className='d-flex gap-1 align-items-center'>
                      <select className='form-control' value={item.pizzaSize} onChange={(e) => handleOrderLineChange(index, 'pizzaSize', e.target.value)}>
                        <option value="Small">Small</option>
                        <option value="Medium">Medium</option>
                        <option value="Large">Large</option>
                      </select>
                      <select value={item.crust} onChange={(e) => handleOrderLineChange(index, 'crust', e.target.value)} className=' form-control'>
                        <option defaultValue={"New hand-tosted"} value="New hand-tosted">New hand-tosted</option>
                        <option value="Wheat thin-crust">Wheat thin-crust</option>
                        <option value="Cheese Brust">Cheese Brust</option>
                        <option value="Fresh pan">Fresh pan</option>
                      </select>
                      <QuantityInput
                        value={item.quantity}
                        onChange={(value) => handleOrderLineChange(index, 'quantity', value)}
                      />
                      <span onClick={() => { handleDeleteOrderLine(item.orderLineId) }} className="material-symbols-outlined">
                        delete
                      </span>

                    </div>
                    <div>
                      {
                        pizza?.pizzaType === "Veg"
                          ?
                          <div className='mt-1'>
                            {
                              VegTopings.map((topping, indx) => (
                                <span key={indx}>
                                  <label style={{ fontSize: "0.6rem" }} className='mx-1'>
                                    {topping}
                                  </label>
                                  <input
                                    checked={toppingsArray.includes(topping)}
                                    type="checkbox"
                                    value={topping}
                                    onChange={(e) => { handleToppingChange(index, topping, e.target.checked) }}
                                  />
                                </span>
                              ))
                            }
                          </div>
                          :
                          <div className='mt-1'>
                            {
                              NonVegToppings.map((topping, indx) => (
                                <span key={indx}>
                                  <label style={{ fontSize: "0.5rem" }} className='mx-1'>
                                    {topping}
                                  </label>
                                  <input
                                    type="checkbox"
                                    checked={toppingsArray.includes(topping)}
                                    value={topping}
                                    onChange={(e) => { handleToppingChange(index, topping, e.target.checked) }}
                                  />

                                </span>
                              ))
                            }
                          </div>
                      }
                    </div>
                  </div>
                )
              })
            }
          </div>
          <div className='mt-2 d-flex justify-content-between'>
            <Button variant="primary" onClick={() => {
              handleSubmit();
              handleClose();
            }}>
              Update
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default UpdateOrderModal;
