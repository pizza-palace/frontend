import Card from 'react-bootstrap/Card';
import { pizzaType } from '../types/pizzaType';
import { useCartContext } from '../context/cart/UseCartContext';
import { useState } from 'react';
import { NonVegToppings, VegTopings } from '../utils/ToppingOptions';
import { getPriceBySize } from '../utils/getPriceBySize';
import { generateNewSubId } from '../utils/generateNewSubId';

const Product: React.FC<{ pizzaData: pizzaType }> = ({ pizzaData }) => {
  const { addToCart } = useCartContext();
  const [selectedSize, setSelectedSize] = useState<string>("Small");
  const [selectedCrust, setSelectedCrust] = useState<string>("New hand-tosted");
  const [quantity, setQuantity] = useState<number>(1);
  const [toppings, setToppings] = useState<string[]>([]);
  let allToppings = "";

  const handleAdd = () => {
    setQuantity(quantity + 1);
  }

  const handleRemove = () => {
    if (quantity >= 2) setQuantity(quantity - 1);
  }

  const handleSelectedCrust = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedCrust(event.target.value);
  }

  const handleSelectedChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedSize(event.target.value);
  }

  const handleToppingChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = event.target;
    setToppings(prevToppings => {
      if (checked) {
        return [...prevToppings, value];
      } else {
        return prevToppings.filter(topping => topping !== value);
      }
    });
  };


  return (
    <Card className=' shadow' style={{ width: '15rem', margin: "10px" }}>
      <Card.Img variant="top" src={pizzaData.imageURL} />
      <Card.Body className=' text-start'>
        <div className="d-flex">
          <Card.Title className=' fs-6 fw-bold'>
            {pizzaData.name}
          </Card.Title>
          <span style={pizzaData.pizzaType === "Veg" ? { color: "green" } : { color: "red" }} className="material-symbols-outlined">
            dialogs
          </span>
        </div>
        <Card.Text style={{ fontSize: "0.65rem", height: "2rem" }}>
          {pizzaData.description}
        </Card.Text>
        <div className='d-flex gap-1'>
          <select value={selectedSize} onChange={handleSelectedChange} style={{ border: "none", borderBottom: "1px solid black", fontSize: "0.6rem" }} className='p-1'>
            <option defaultValue={"Small"} value="Small">Small  ₹{pizzaData.priceRegular}</option>
            <option value="Medium">Medium  ₹{pizzaData.priceMedium}</option>
            <option value="Large">Large  ₹{pizzaData.priceLarge}</option>
          </select>
          <select value={selectedCrust} onChange={handleSelectedCrust} style={{ border: "none", borderBottom: "1px solid black", fontSize: "0.6rem" }} className='p-1'>
            <option defaultValue={"New hand-tosted"} value="New hand-tosted">New hand-tosted</option>
            <option value="Wheat thin-crust">Wheat thin-crust</option>
            <option value="Cheese Brust">Cheese Brust</option>
            <option value="Fresh pan">Fresh pan</option>
          </select>
        </div>
        {
          pizzaData.pizzaType === "Veg"
            ?
            <div className='mt-1'>
              {
                VegTopings.map((topping, index) => (
                  <span key={index}>
                    <label style={{ fontSize: "0.6rem" }} className='mx-1'>
                      {topping}
                    </label>
                    <input
                      type="checkbox"
                      value={topping}
                      onChange={handleToppingChange}
                    />
                  </span>
                ))
              }
            </div>
            :
            <div className='mt-1'>
              {
                NonVegToppings.map((topping, index) => (
                  <span key={index}>
                    <label style={{ fontSize: "0.5rem" }} className='mx-1'>
                      {topping}
                    </label>
                    <input
                      type="checkbox"
                      value={topping}
                      onChange={handleToppingChange}
                    />

                  </span>
                ))
              }
            </div>
        }

        <div className='d-flex justify-content-between mt-2 mb-1'>
          <div className='d-flex align-items-center border border-secondary rounded-1'>
            <span onClick={handleRemove} style={{ cursor: "pointer" }} className="material-symbols-outlined ps-1 text-danger">
              remove
            </span>
            <span className='pt-1 px-3 fw-bold'>{quantity}</span>
            <span onClick={handleAdd} style={{ cursor: "pointer" }} className="material-symbols-outlined pe-1 text-success">
              add
            </span>
          </div>
          <button
            onClick={
              () => {

                for (let i = 0; i < toppings.length; i++) {
                  if (i === toppings.length - 1) allToppings += toppings[i];
                  else allToppings += (toppings[i] + "-");
                }

                let cartItem = {
                  pizzaId: pizzaData.pizzaId,
                  name: pizzaData.name,
                  pizzaType: pizzaData.pizzaType,
                  description: pizzaData.description,
                  pizzaSize: selectedSize,
                  crust: selectedCrust,
                  quantity: quantity,
                  price: getPriceBySize(selectedSize, pizzaData),
                  toppings: allToppings,
                  subId: generateNewSubId()
                }
                addToCart(cartItem)
              }}
            className=' fw-bold btn btn-outline-success btn-sm'>Add to cart</button>
        </div>
      </Card.Body>
    </Card >
  );
}

export default Product;