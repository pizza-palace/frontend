type PropType = {
  value: number
  onChange: (value: number) => void
}

export const QuantityInput = ({ value, onChange }: PropType) => {
  const handleIncrement = () => {
    if (value < 20) {
      onChange(value + 1);
    }
  };

  const handleDecrement = () => {
    if (value > 1) {
      onChange(value - 1);
    }
  };

  return (
    <div className="d-flex align-items-center border border-secondary rounded-1">
      <span onClick={handleDecrement} style={{ cursor: "pointer" }} className="material-symbols-outlined ps-1 text-danger">
        remove
      </span>
      <input
        type="number"
        className="w-50 text-center border border-0 fw-bold fs-6"
        value={value}
        onChange={(e) => {
          const value = parseInt(e.target.value, 10);
          if (value > 0 && value <= 20) {
            onChange(value);
          }
        }}
        readOnly
      />
      <span onClick={handleIncrement} style={{ cursor: "pointer" }} className="material-symbols-outlined pe-1 text-success">
        add
      </span>
    </div>
  );

}