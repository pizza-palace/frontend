import { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { CartType } from '../types/CartType';
import { QuantityInput } from './QuantityInput';
import { useCartContext } from '../context/cart/UseCartContext';
import { getPriceBySize } from '../utils/getPriceBySize';
import { UsePizzaContext } from '../context/pizza/UsePizzaContext';
import { NonVegToppings, VegTopings } from '../utils/ToppingOptions';

type PropType = {
  CartItem: CartType;
}


function UpdateCartItem({ CartItem }: PropType) {
  
  const { editCartItem } = useCartContext();
  const { getPizza } = UsePizzaContext();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [size, setSize] = useState<string>(CartItem.pizzaSize);
  const [crust, setCrust] = useState<string>(CartItem.crust);
  const [quantity, setQuntity] = useState<number>(CartItem.quantity);
  const [toppings, setToppings] = useState<string[]>(CartItem.toppings.split("-"));
  let allToppings = "";
  useEffect(() => {
    setToppings(CartItem.toppings.split("-"));
    
  }, [show])
  
  const handleUpdate = () => {
    const pizza = getPizza(CartItem.pizzaId);
    let updatedPrice = 0;
    
    for (let i = 0; i < toppings.length; i++) {
      if(toppings[i].length > 0) {
        if (i === toppings.length - 1) allToppings += toppings[i];
        else allToppings += (toppings[i] + "-");
      }
    }
    console.log(pizza);
    
    if (pizza) {
      updatedPrice += getPriceBySize(size, pizza);
    }
    const updatedCartItem = { ...CartItem, pizzaSize: size, quantity: quantity, crust: crust, price: updatedPrice, toppings:allToppings };
    editCartItem(CartItem.subId, updatedCartItem);
  }

  const handleToppingChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, checked } = event.target;
    setToppings(prevToppings => {
      if (checked) {
        return [...prevToppings, value];
      } else {
        return prevToppings.filter(topping => topping !== value);
      }
    });
  };


  return (
    <>
      <span onClick={handleShow} className="material-symbols-outlined pointer text-warning">
        edit
      </span>

      <Modal show={show} onHide={handleClose}>

        <Modal.Body>
          <div id='update-order-div' className='d-flex flex-column gap-1 align-items-center justify-content-between border border-black p-2 rounded-1'>
            <p className='fw-bold'>{CartItem.name}</p>
            <div className='d-flex gap-1 align-items-center'>
              <select className='form-control' value={size} onChange={(e) => { setSize(e.target.value) }}>
                <option value="Small">Small</option>
                <option value="Medium">Medium</option>
                <option value="Large">Large</option>
              </select>
              <select className='form-control' value={crust} onChange={(e) => { setCrust(e.target.value) }}>
                <option defaultValue={"New hand-tosted"} value="New hand-tosted">New hand-tosted</option>
                <option value="Wheat thin-crust">Wheat thin-crust</option>
                <option value="Cheese Brust">Cheese Brust</option>
                <option value="Fresh pan">Fresh pan</option>
              </select>
              <QuantityInput
                value={quantity}
                onChange={(value) => { setQuntity(value) }}
              />
            </div>
            <div>
              {
                CartItem.pizzaType === "Veg"
                  ?
                  <div className='mt-1'>
                    {
                      VegTopings.map((topping, index) => (
                        <span key={index}>
                          <label style={{ fontSize: "0.6rem" }} className='mx-1'>
                            {topping}
                          </label>
                          <input
                            checked={toppings.includes(topping)}
                            type="checkbox"
                            value={topping}
                            onChange={handleToppingChange}
                          />
                        </span>
                      ))
                    }
                  </div>
                  :
                  <div className='mt-1'>
                    {
                      NonVegToppings.map((topping, index) => (
                        <span key={index}>
                          <label style={{ fontSize: "0.5rem" }} className='mx-1'>
                            {topping}
                          </label>
                          <input
                            type="checkbox"
                            checked={toppings.includes(topping)}
                            value={topping}
                            onChange={handleToppingChange}
                          />

                        </span>
                      ))
                    }
                  </div>
              }
            </div>
          </div>
          <button onClick={() => { handleUpdate(); handleClose(); }} className='btn btn-primary mt-2'>Update</button>
        </Modal.Body>

      </Modal>
    </>
  );
}

export default UpdateCartItem;