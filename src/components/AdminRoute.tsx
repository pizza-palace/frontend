import React from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../context/Auth/AuthContext";

const AdminRoute: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const { isAuthenticated, isAdmin } = useAuth();
  
    
  if (isAuthenticated && isAdmin) {
    return <>{children}</>;
  } else if(isAuthenticated && !isAdmin){
    return <Navigate to="/" />;
  } else {
    return <Navigate to="/login" />;
  }
  

  
};

export default AdminRoute;
