import Product from "../components/product";
import { pizzaType } from "../types/pizzaType";
import { UsePizzaContext } from "../context/pizza/UsePizzaContext";

const MainPage = () => {
    const { pizzaList, addPizzaList } = UsePizzaContext();
    return (
        <div style={{ paddingTop: "6rem" }}>
            <p className="text-center fs-1 pt-3 fw-medium text-primary">Menu</p>
            <div className="d-flex align-items-center justify-content-center flex-wrap pt-5 gap-4">
                {
                    pizzaList.map((item: pizzaType, index: number) => (
                        <Product key={index} pizzaData={item} />
                    ))
                }
            </div>
        </div>
    );
}

export default MainPage;