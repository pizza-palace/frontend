import { useEffect, useState } from 'react'
import { OrderType } from '../types/OrderType'
import pizzaService from '../services/pizza.service';
import { useUserContext } from '../context/user/UseUserContext';
import OrderDetailsModal from '../components/OrderDetailsModal';
import { ToastContainer, toast } from 'react-toastify';
import UpdateOrderModal from '../components/UpdateOrderModal';


export const MyOrders = () => {
  
  const { userData } = useUserContext();
  const [orderList, setOrderList] = useState<OrderType[]>([]);
  

  useEffect(() => {
    pizzaService.getAllPizzaOrdersByCustomerId(userData.customerId)
      .then((res: any) => {
        setOrderList(res.data.data);
      })
  }, [orderList])

  

  const isEditable = (orderDateTime: string) => {
    const orderTime = new Date(orderDateTime).getTime();
    const currTime = new Date().getTime();
    return (currTime - orderTime) < (15 * 60 * 1000);
    // return true;
  }

  const handleDelete = async (id: number | undefined, dateTime: string) => {

    if (isEditable(dateTime)) {
      await pizzaService.deleteOrderLine(id)
        .then((res) => {
        })
        .catch((err) => {
        })

      await pizzaService.deleteOrder(id)
        .then((res) => {
          toast.success("Order deleted successfully");
        })
        .catch((err) => {
        })
    }
  }


  return (
    <div style={{ paddingTop: "6rem" }} className='d-flex flex-column align-items-center gap-3'>
      
      <p className='fs-2'>Orders</p>
      {
        orderList.length === 0
          ?
          <p className='text-danger'>You don't have orders</p>
          :
          orderList.map((item, index) => (
            <div key={index} className=' bg-light w-50 p-3 rounded-3 shadow'>
              <div className='d-flex justify-content-between'>
                <p className='fw-bold'>Order No - {item.orderId}</p>
                <p className='fw-bold'>Status - <span style={{ color: "green" }}>{item.status}</span></p>
              </div>
              <div>
                <p><span className='fw-bold'>Address - </span><span style={{ fontSize: "0.8rem" }}>{item.deliveryAddress}</span></p>
              </div>
              <div className='d-flex justify-content-between'>
                <p><span className='fw-bold'>Total - </span> &#8377;{item.totalAmount}</p>
                <div className='d-flex align-items-center gap-4'>
                  <span onClick={() => { handleDelete(item.orderId, item.orderDateTime) }} style={isEditable(item.orderDateTime) ? {} : { display: "none" }} className="material-symbols-outlined text-danger pointer">
                    delete
                  </span>
                  
                  <UpdateOrderModal isEditable={isEditable} orderDateTime={item.orderDateTime} id={item.orderId}/>
                  <OrderDetailsModal id={item.orderId} />
                </div>
              </div>
            </div>
          ))
      }
      <ToastContainer />
    </div>
  )
}
