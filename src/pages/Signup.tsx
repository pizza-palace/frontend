import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { ToastContainer, toast } from "react-toastify";
import * as Yup from 'yup';
import userService from '../services/user.service';
import { AxiosError } from 'axios';
import { useNavigate } from 'react-router-dom';

interface SignupFormValues {
    firstName: string;
    lastName: string;
    address: string;
    phoneNumber: string;
    emailAddress: string;
    password: string;
}

const Signup: React.FC = () => {
    const navigate = useNavigate();
    const initialValues: SignupFormValues = {
        firstName: '',
        lastName: '',
        address: '',
        phoneNumber: '',
        emailAddress: '',
        password: ''
    };

    const validationSchema = Yup.object({
        firstName: Yup.string().required('First name is required'),
        emailAddress: Yup.string().email('Invalid email address').required('Email is required'),
        password: Yup.string().min(8, 'Password must be at least 8 characters').required('Password is required'),
        phoneNumber: Yup.string()
            .matches(/^[0-9]{10}$/, 'Phone number must be 10 digits')
    });

    const onSubmit = (values: SignupFormValues) => {
        userService.createUser(values)
            .then((res: any) => {
                toast.success("Registration Successfull")
                
                setTimeout(()=>{
                    navigate("/login");
                }, 3000);
            })
            .catch((err) => {
                if(err instanceof AxiosError){
                    if(err.response){
                        toast.error(err.response.data.error.message);
                    }
                }
            })
    };

    return (
        <div className='d-flex align-items-center'>
            <div style={{ width: "30vw", marginTop: "6rem" }} className=" container mb-5">
                <h2 className="text-center">Signup</h2>
                <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                    <Form>
                        <div className="mb-3">
                            <label htmlFor="firstName" className="form-label">First Name</label>
                            <Field type="text" id="firstName" name="firstName" className="form-control" />
                            <ErrorMessage name="firstName" component="div" className="text-danger" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="lastName" className="form-label">Last Name</label>
                            <Field type="text" id="lastName" name="lastName" className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="address" className="form-label">Address</label>
                            <Field type="text" id="address" name="address" className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="phoneNumber" className="form-label">Phone Number</label>
                            <Field type="text" id="phoneNumber" name="phoneNumber" className="form-control" />
                            <ErrorMessage name="phoneNumber" component="div" className="text-danger" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="emailAddress" className="form-label">Email</label>
                            <Field type="email" id="emailAddress" name="emailAddress" className="form-control" />
                            <ErrorMessage name="emailAddress" component="div" className="text-danger" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="password" className="form-label">Password</label>
                            <Field type="password" id="password" name="password" className="form-control" />
                            <ErrorMessage name="password" component="div" className="text-danger" />
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </Form>
                </Formik>
            </div>
            <ToastContainer/>
        </div>
    );
};

export default Signup;
