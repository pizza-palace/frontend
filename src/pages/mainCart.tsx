import { useCartContext } from "../context/cart/UseCartContext";
import { OrderType } from "../types/OrderType";
import PizzaDataService from "../services/pizza.service"
import { AllToppings } from "../utils/ToppingOptions";
import { useUserContext } from "../context/user/UseUserContext";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import { AxiosError } from "axios";
import UpdateCartItem from "../components/UpdateCartItem";
import { useAuth } from "../context/Auth/AuthContext";

const MainCart = () => {
    const { cartData, removeFromCart, emptyCart } = useCartContext();
    const { userData } = useUserContext();
    const { isAuthenticated } = useAuth();
    const [address, setAddress] = useState<string>("");
    const navigate = useNavigate();

    let grandTotal: number = 0;

    const calculateGrandTotal = () => {

        for (let i = 0; i < cartData.length; i++) {
            grandTotal += (cartData[i].quantity * cartData[i].price);
            let toppingsArray = cartData[i].toppings.split("-");

            for (let j = 0; j < toppingsArray.length; j++) {
                if (toppingsArray[j].length > 0) grandTotal += AllToppings[toppingsArray[j]];
            }
        }
    }
    calculateGrandTotal();

    useEffect(() => {
        calculateGrandTotal();
    }, [cartData]);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setAddress(event.target.value);
    }

    const handlePlaceOrder = async (orderData: OrderType) => {

        try {
            const res = await PizzaDataService.createOrder(orderData);
            const newOrderId = res.data.data.orderId;

            const orderLinePromises = cartData.map((item) => {

                PizzaDataService.createOrderLine({
                    orderId: newOrderId,
                    pizzaId: item.pizzaId,
                    pizzaSize: item.pizzaSize,
                    quantity: item.quantity,
                    totalPrice: (item.price * item.quantity),
                    crust: item.crust,
                    toppings: item.toppings
                })
            });

            await Promise.all(orderLinePromises);
            console.log(orderLinePromises);
            
            toast.success("order placed successfully");
            emptyCart();
            setAddress("");

        } catch (err) {
            console.log(err);
            if (err instanceof AxiosError) {
                if (err.response) {
                    toast.error(err.response.data.error.message[0]);
                }
            }
        }
    }

    return (
        <div style={cartData.length === 0 ? { paddingTop: "6rem", height: "95vh" } : { paddingTop: "6rem" }} className=" d-flex justify-content-center">
            {
                cartData.length === 0
                    ?
                    <div className=" fs-3 text-danger p-5">Ooops! Cart is Empty.</div>
                    :
                    (
                        <div className="d-flex flex-column gap-4">
                            <div className="d-flex flex-column gap-4 justify-content-evenly shadow bg-light rounded-4 p-3">
                                <div className="d-flex justify-content-center gap-4">
                                    <span className="fs-2 text-success">Grand Total :  ₹{grandTotal}</span>
                                    <input value={address} onChange={handleChange} className=" form-control w-50" type="text" placeholder="Enter Address" required />
                                </div>
                                <div className="d-flex align-items-center flex-column gap-2">
                                    <button
                                        onClick={() => {
                                            if (isAuthenticated) {
                                                handlePlaceOrder({
                                                    customerId: userData.customerId,
                                                    totalAmount: grandTotal,
                                                    orderDateTime: new Date().toISOString(),
                                                    deliveryAddress: address
                                                })
                                            } else {
                                                navigate("/login");
                                            }
                                        }}
                                        className="btn btn-primary">Place Order</button>
                                </div>
                            </div>
                            {
                                cartData.map((item, index) => (
                                    <div key={index} className=" bg-light p-3 rounded shadow">
                                        <div className="d-flex justify-content-between">
                                            <div>
                                                <p className=" fw-bold fs-5">
                                                    {item.name}
                                                    <span style={item.pizzaType === "Veg" ? { color: "green" } : { color: "red" }} className=" ms-3 material-symbols-outlined">
                                                        dialogs
                                                    </span>
                                                </p>
                                                <p style={{ fontSize: "0.7rem" }}>{item.description}</p>
                                                <p className=" fw-medium">{item.quantity} | {item.pizzaSize} | {item.crust}</p>
                                            </div>
                                            <div>
                                                <p className=" fw-bold fs-5 text-end">₹{item.price}/-</p>
                                                <p className=" ms-4">
                                                    <span onClick={() => { removeFromCart(item.subId) }} className="material-symbols-outlined text-danger pointer me-3">
                                                        delete
                                                    </span>
                                                    <UpdateCartItem CartItem={item} />
                                                </p>
                                            </div>
                                        </div>
                                        {
                                            item.toppings.length !== 0
                                            &&
                                            <div style={{ borderTop: "solid grey 1px" }} className="pt-1">
                                                <span style={{ fontSize: "0.8rem" }} className="fw-medium">Added Toppings : </span>
                                                {
                                                    item.toppings.split("-").map((topping, index) => (
                                                        <span style={{ fontSize: "0.6rem" }} key={index}>{topping}, </span>
                                                    ))
                                                }
                                            </div>
                                        }
                                    </div>
                                ))
                            }
                        </div>
                    )
            }
            <ToastContainer />
        </div>
    )
}


export default MainCart;