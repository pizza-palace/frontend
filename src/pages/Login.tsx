import React, { useState } from 'react';
import { Form, Button, Container, Row, Col, Alert } from 'react-bootstrap';
import userService from '../services/user.service';
import { ToastContainer, toast } from 'react-toastify';
import { useUserContext } from '../context/user/UseUserContext';
import { useNavigate } from 'react-router-dom';
import { AxiosError } from 'axios';
import { useAuth } from '../context/Auth/AuthContext';

const Login: React.FC = () => {
  const { addUser } = useUserContext();
  const { login, addAdmin } = useAuth();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [error, setError] = useState<string>('');
  const navigate = useNavigate();

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
  
    if (!email || !password) {
      setError('Please fill in all fields');
      return;
    }
  
    const handleLoginSuccess = (res: any, isUser: boolean) => {
      const data = res.data.data;
      toast.success("Login successful");
      login();
      addUser({
        customerId: isUser ? data.customerId : data.id,
        firstName: isUser ? data.firstName : data.name,
        emailAddress: data.emailAddress,
      });
      setTimeout(() => {
        navigate(isUser ? "/" : "/admin/home");
      }, 700);
    };
  
    const handleLoginError = (err: any) => {
      if (err instanceof AxiosError && err.response) {
        toast.error(err.response.data.error.message);
      }
    };
  
    try {
      const userRes = await userService.getUser(email);
      if (userRes.data.data.password === password) {
        handleLoginSuccess(userRes, true);
        return;
      } else {
        toast.error("Wrong password");
      }
    } catch (err) {
      try {
        const adminRes = await userService.getAdmin(email);
        if (adminRes.data.data.password === password) {
          addAdmin();
          handleLoginSuccess(adminRes, false);
          return;
        } else {
          toast.error("Wrong password");
        }
      } catch (err) {
        
      }
      handleLoginError(err);
    }
  
    setEmail("");
    setPassword("");
    setError('');
  };
  

  return (
    <Container style={{ paddingTop: "6rem" }} className='mb-5'>
      <Row className="justify-content-md-center">
        <Col md="6">
          <h2 className='text-center'>Login</h2>
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit} className='d-flex gap-3 flex-column'>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Button className='w-25' variant="primary" type="submit">
              Login
            </Button>
          </Form>
        </Col>
      </Row>
      <ToastContainer />
    </Container>
  );
};

export default Login;
