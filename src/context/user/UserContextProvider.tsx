import { useState, useEffect } from "react";
import { ComponenetProp } from "../../types/ComponentProp";
import { UserType } from "../../types/UserType";
import UserContext from "./UserContext";

export const UserContextProvider = ({ children }: ComponenetProp) => {
  const [userData, setUserData] = useState<UserType>(() => {
    const savedUserData = localStorage.getItem('userData');
    return savedUserData ? JSON.parse(savedUserData) : ({} as UserType);
  });

  const addUser = (user: UserType) => {
    setUserData(user);
    localStorage.setItem('userData', JSON.stringify(user));
  };

  const removeUser = () => {
    setUserData({} as UserType);
    localStorage.removeItem('userData');
  };

  useEffect(() => {
    const savedUserData = localStorage.getItem('userData');
    if (savedUserData) {
      setUserData(JSON.parse(savedUserData));
    }
  }, []);

  return (
    <UserContext.Provider value={{ userData, addUser, removeUser }}>
      {children}
    </UserContext.Provider>
  );
};
