import { createContext } from "react";
import { UserType } from "../../types/UserType"

type UserContextType = {
    userData : UserType
    addUser : (user : UserType) => void;
    removeUser : () => void;
}

const UserContext = createContext<UserContextType | undefined>(undefined);

export default UserContext;