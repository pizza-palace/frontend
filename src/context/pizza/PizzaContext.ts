import { createContext } from "react"
import { pizzaType } from "../../types/pizzaType"

type PizzaContextType = {
    pizzaList : pizzaType[]
    addPizzaList : (pizzaList : pizzaType[]) => void
    getPizza : (pizzaId : number) => pizzaType | undefined
}

const PizzaContext = createContext<PizzaContextType | undefined>(undefined);

export default PizzaContext;