import { useEffect, useState } from 'react'
import { ComponenetProp } from '../../types/ComponentProp'
import PizzaContext from './PizzaContext'
import { pizzaType } from '../../types/pizzaType';
import PizzaDataService from "../../services/pizza.service"


export const PizzaContextProvider = ({children} : ComponenetProp) => {
    const [pizzaList, setPizzaList] = useState<pizzaType[]>([]);
    
    const addPizzaList = (pizzaList :pizzaType[]) => {
        setPizzaList(pizzaList);
    }

    const getPizza = (pizzaId : number) => {
        return pizzaList.find((pizza) => pizza.pizzaId === pizzaId);
    }

    useEffect(() => {
      PizzaDataService.getAllPizza().then((res: any) => {
          addPizzaList(res.data.data);
      })
  }, [])

  return (
    <PizzaContext.Provider value={{pizzaList, addPizzaList, getPizza}}>
        {children}
    </PizzaContext.Provider>
  )
}
