import { useContext } from 'react'
import PizzaContext from './PizzaContext'

export const UsePizzaContext = () => {
    const context = useContext(PizzaContext);
    if (!context) {
        throw Error("UsePizzaContext must be used inside the PizzaContextProvide");
    }
    return context;
}
