import React, { createContext, useState, useContext, useEffect, ReactNode } from 'react';

interface AuthContextType {
  isAuthenticated: boolean;
  isAdmin : boolean;
  login: () => void;
  logout: () => void;
  addAdmin : () => void;
  removeAdmin : () => void;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const AuthProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(() => {
    const savedAuth = localStorage.getItem('isAuthenticated');
    return savedAuth ? JSON.parse(savedAuth) : false;
  });

  const [isAdmin, setIsAdmin] = useState<boolean>(()=>{
    const savedIsAdmin = localStorage.getItem('isAdmin');
    return savedIsAdmin ? JSON.parse(savedIsAdmin) : false;
  })

  const addAdmin = () => {
    setIsAdmin(true);
    localStorage.setItem('isAdmin', JSON.stringify(true));
  }

  const removeAdmin = () => {
    setIsAdmin(false);
    localStorage.setItem('isAdmin', JSON.stringify(false));
  }


  const login = () => {
    setIsAuthenticated(true);
    localStorage.setItem('isAuthenticated', JSON.stringify(true));
  };

  const logout = () => {
    setIsAuthenticated(false);
    localStorage.removeItem('isAuthenticated');
  };

  useEffect(() => {
    const savedAuth = localStorage.getItem('isAuthenticated');
    if (savedAuth) {
      setIsAuthenticated(JSON.parse(savedAuth));
    }
    const savedIsAdmin = localStorage.getItem('isAdmin');
    if (savedIsAdmin) {
      setIsAdmin(JSON.parse(savedIsAdmin));
    }
  }, []);

  return (
    <AuthContext.Provider value={{ isAuthenticated, login, logout, addAdmin, removeAdmin, isAdmin }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};
