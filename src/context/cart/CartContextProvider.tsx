import { useState, useEffect } from 'react';
import CartContext from './CartContext';
import { CartType } from '../../types/CartType';
import { ComponenetProp } from '../../types/ComponentProp';

const CartContextProvider = ({ children }: ComponenetProp) => {
    const [cartData, setCartData] = useState<CartType[]>(() => {
        const storedCartData = localStorage.getItem('cartData');
        return storedCartData ? JSON.parse(storedCartData) : [];
    });

    useEffect(() => {
        localStorage.setItem('cartData', JSON.stringify(cartData));
    }, [cartData]);

    const addToCart = (item: CartType) => {
        setCartData(prevCart => [...prevCart, item]);
        localStorage.setItem('cartData', JSON.stringify(cartData));
    };

    const removeFromCart = (id: number) => {
        setCartData(prevCart => prevCart.filter(item => item.subId !== id));
        localStorage.setItem('cartData', JSON.stringify(cartData));
    };

    const emptyCart = () => {
        setCartData([]);
        localStorage.setItem('cartData', JSON.stringify(cartData));
    };

    const editCartItem = (id: number, updatedItem: Partial<CartType>) => {
        setCartData(prevCart =>
            prevCart.map(item =>
                item.subId === id ? { ...item, ...updatedItem } : item
            )
        );
        localStorage.setItem('cartData', JSON.stringify(cartData));
        console.log(localStorage.getItem('cartData'));
        
    };

    return (
        <CartContext.Provider value={{ cartData, addToCart, removeFromCart, emptyCart, editCartItem }}>
            {children}
        </CartContext.Provider>
    );
}

export default CartContextProvider;
