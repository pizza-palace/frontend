import { createContext } from "react";
import { CartType } from "../../types/CartType";

type CartContextType = {
  cartData: CartType[];
  addToCart: (item: CartType) => void;
  removeFromCart: (id: number) => void;
  emptyCart: () => void;
  editCartItem : (id: number, updatedItem: Partial<CartType>) => void;
}
const CartContext = createContext<CartContextType | undefined>(undefined);

export default CartContext;