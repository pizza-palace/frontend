import './App.css';
import Content from './common/content';
import Footer from './common/footer';
import NavbarComponent from './common/navbar';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import "react-toastify/dist/ReactToastify.css";
import CartContextProvider from './context/cart/CartContextProvider';
import { BrowserRouter } from 'react-router-dom';
import { UserContextProvider } from './context/user/UserContextProvider';
import { PizzaContextProvider } from './context/pizza/PizzaContextProvider';
import { AuthProvider } from './context/Auth/AuthContext';



function App() {

  return (
    <div className="App" style={{ backgroundColor: "#EEE" }}>
      <BrowserRouter>
        <CartContextProvider>
          <UserContextProvider>
            <PizzaContextProvider>
              <AuthProvider>
                <NavbarComponent />
                <Content />
              </AuthProvider>
            </PizzaContextProvider>
          </UserContextProvider>
        </CartContextProvider>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
