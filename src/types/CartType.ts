export type CartType = {
    pizzaId : number
    subId : number
    name : String
    pizzaType : String
    description : string
    pizzaSize : string
    crust : string
    quantity : number
    price : number
    toppings : string
}