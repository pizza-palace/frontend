import { ReactNode } from "react"

export type ComponenetProp = {
    children: ReactNode
}