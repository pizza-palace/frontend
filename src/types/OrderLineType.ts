export type OrderLineType = {
    orderLineId ?: number
    orderId : number
    pizzaId : number
    pizzaSize : string
    quantity : number 
    totalPrice : number
    crust : string
    toppings : string
}