export type SignupType = {
    firstName: string;
    lastName: string;
    address: string;
    phoneNumber: string;
    emailAddress: string;
    password: string;
}