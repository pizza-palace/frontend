export type pizzaType = {
    pizzaId : number
    name : string
    description : string
    pizzaType : string 
    imageURL : string
    priceRegular : number
    priceMedium : number
    priceLarge : number
}