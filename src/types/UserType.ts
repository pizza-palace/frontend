export type UserType = {
    customerId : number
    firstName : string
    emailAddress : string
}