export type Pizza = {
    name: string;
    pizzaType: string;
    description: string;
    imageURL : string;
    priceRegular: number | '';
    priceMedium: number | '';
    priceLarge: number | '';
  }