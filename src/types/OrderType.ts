export type OrderType = {
    orderId ?: number
    status ?: string
    customerId : number
    totalAmount : number
    orderDateTime : string
    deliveryAddress : string
}